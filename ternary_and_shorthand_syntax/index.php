<!doctype html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Ternary & Shorthand Syntax</title>
</head>
<body>
<div class="container">
    <h2>Ternary & Shorthand Syntax</h2>
    <p>PHP Front To Back - [Part 13] Ternary & Shorthand Syntax</p>
    <a href="https://www.youtube.com/watch?v=NUq0ZT54zvw&list=PLillGF-Rfqbap2IB6ZS4BBBcYPagAjpjn&index=13">Link</a>
    <br>
    <br>
    <?php
    $loggedIn = true;
if($loggedIn) {
    echo "You are logged in";
} else {
    echo "You aren't logged in";
}
    ?>
    <br>
    <br>
    <?php
    $text = "Ternary operator says: ";
    echo ($loggedIn) ? $text ."You are logged in!" : $text . "Your are not logged in";
    ?>
    <br>
    <br>
    <h4>Nested ternarys</h4>
    <?php
    $age = 7;
    $score = 9;
    echo "Your score is: ".($score > 10 ? ($age > 10 ? "Average" : "Exceptional"):($age > 10 ? "Horrible":"Average"));
    ?>
    <br>
    <br>
    <h2>Embedding HTML in PHP</h2>
    <br>
    <?php if($loggedIn) { ?>
    <h4>Logged in User is true</h4>
    <?php } else {?>
        <h4>Welcome guest</h4>
    <?php } ?>
    <br>
    <br>
    <h2>Better looking code</h2>
    <?php if($loggedIn): ?>
    <h4>Logged in successful</h4>
    <?php else: ?>
    <h4>Welcome Guest</h4>
    <?php endif; ?>
    <?php
    $array = [1,2,3,4,5];
    ?>
    <br>
    <br>
    <?php foreach($array as $val): ?>
    <?php echo $val; ?>
    <?php endforeach; ?>
    <br>
    <br>
    <?php for($i = 0; $i<10; $i++): ?>
    <li><?php echo $i; ?></li>
    <?php endfor; ?>
</div>
</body>
</html>