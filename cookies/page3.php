<?php

$msg = "";
$msgClass = "";

if(isset($_POST["submit"])) {

    $name = htmlentities($_POST["name"]);
    $email = htmlentities($_POST["email"]);

    $rawData = ["name" => $name, "email" => $email];

    $serializedData = serialize($rawData);

    setcookie("arrayCookie", $serializedData, time() + 18000);

    header("Refresh: 1");

}

if(isset($_COOKIE["arrayCookie"])){
$cookie = unserialize($_COOKIE["arrayCookie"]);
$msg = "Name: " . $cookie["name"] . "<br> Email: ". $cookie["email"];
$msgClass = "alert-success";
}

if(isset($_POST["delete"])){
    setcookie("arrayCookie", null, -1 );
    header("Refresh:0");
}

?>

<!doctype html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <title>Cookies - Page 3 | Arrays to cookies</title>
</head>
<body>
<?php require 'inc/header.php'; ?>

<div class="container">
    <br>

    <h3>Arrays to cookies</h3>
    <br>
    <form method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
        <div class="form-group">
            <label>Name</label>
            <input type="text" name="name" class="form-control">
        </div>
        <div>
            <label>Email</label>
            <input type="text" name="email" class="form-control">
        </div>
        <br>
        <button type="submit" name="submit" class="btn btn-primary">Abschicken</button>
    </form>
    <br>
    <a href="index.php">Back to index page</a>
    <br>
    <br>
    <?php if($msg != ""): ?>
        <div class="alert <?php echo $msgClass?>"> <?php echo $msg; ?> </div>
    <?php endif; ?>
    <br>
    <form method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
        <button type="submit" name="delete" class="btn btn-primary">Cookie löschen</button>
    </form>
</div>
</body>
</html>
