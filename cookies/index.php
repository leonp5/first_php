<?php
if(isset($_POST["submit"])){
$name = htmlentities($_POST["name"]);
// cookie name, value, expire time
setcookie("phptutcookie", $name, time()+1800);

header("Location: page2.php");
}

?>

<!doctype html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Cookies</title>
</head>
<body>
<?php require 'inc/header.php'; ?>
    <br>
    <br>
<div class="container">
    <p>PHP Front To Back [Part 17] - Cookies</p>
    <a href="https://www.youtube.com/watch?v=RzMjwICWKr4&list=PLillGF-Rfqbap2IB6ZS4BBBcYPagAjpjn&index=17">Link</a>
<br>
<br>
<br>
<div class="container">
    <form method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
        <div class="form-group">
            <label>Name</label>
            <input type="text" name="name" class="form-control">
        </div>
        <br>
        <button type="submit" name="submit" class="btn btn-primary">Abschicken</button>
    </form>
    <br>
    <a href="page3.php">Save arrays to cookies</a>
</div>
</body>
</html>