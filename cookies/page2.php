<?php

$msg = "";
$msgClass = "";

if(isset($_COOKIE["phptutcookie"])){
    $msg = "User " . "'{$_COOKIE["phptutcookie"]}'" . " is set! <br>";
    $msgClass = "alert-success";
} else {
    $msg = "No cookie here";
    $msgClass = "alert-warning";
}

if(isset($_POST["delete"])){
    setcookie("phptutcookie", null, -1 );
    header("Refresh:0");
}

?>

<!doctype html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Cookies - Page 2</title>
</head>
<body>
<?php require 'inc/header.php'; ?>
<div class="container">

    <?php if($msg != ""): ?>
        <div class="alert <?php echo $msgClass?>"> <?php echo $msg; ?> </div>
    <?php endif; ?>
    <form method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
    <button type="submit" name="delete" class="btn btn-primary">Cookie löschen</button>
    </form>
<br>
<br>
    <a href="page3.php">Save arrays to cookies</a>
    <br>
    <br>
    <p>or</p>
    <br>
<a href="index.php">back</a>
</div>
</body>
</html>
