
<!doctype html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Common String functions</title>
</head>
<body>
<div class="container">
    <h2>Common String functions</h2>
    <p>PHP Front To Back [Part 12] - Common String Functions</p>
    <a href="https://www.youtube.com/watch?v=z32BOHVWplU&list=PLillGF-Rfqbap2IB6ZS4BBBcYPagAjpjn&index=12">Link</a>
    <br>
    <br>
    <h3>substr()</h3>
    <p>Returns a portion of a string</p>
    <br>
<?php
$output = substr("Hello", 0, 3);
echo $output;
?>
    <br>
    <br>
    <h3>strlen("Hello World")</h3>
    <p>Returns the length of a string</p>
    <?php
    $output1 = strlen("Hello World");
    echo $output1;
    ?>
    <h3>strpos("Hello World")</h3>
    <p>Finds the position of the first occurence of a sub string</p>
    <?php
    $output2 = strpos("Hello World", "o");
    echo $output2;
    ?>
    <h3>trim("Hello World")</h3>
    <p>Strips whitespace</p>
    <?php
    $text = "Hello World                ";
    var_dump($text);
    ?>
    <br>
    <?php
    $trimmed =trim($text);
    var_dump($trimmed);
    ?>
    <h3>strtoupper/strtolower</h3>
    <p>Makes everything uppercase/lowercase</p>
    <?php
    $output4 = strtoupper("Hello World");
    echo $output4;
    ?>
    <br>
    <?php  $output3 = strtolower("Hello World");
    echo $output3;
    ?>
    <br>
    <h3>ucwords("hello world")</h3>
    <p>Capitalize every word</p>
    <?php
    $output5 = ucwords("hello world");
    echo $output5;
    ?>
    <br>
    <br>
    <h3>str_replace()</h3>
    <p>Replace all occurences of a search string with a replacement</p>
    <?php
    $text2 = "Hello World";
    $output6 = str_replace("World", "Everyone", $text2);
    echo $output6;
    ?>
    <br>
    <br>
    <h3>is_string()</h3>
    <p>Check if string</p>
    <?php
    $val = "Hello";
    $output7 = is_string($val);
    echo $output7;
    ?>
    <br>
    <br>
    <?php
    $values = [true, false, null, "abc", 33, "33", 22.4, "22.4", "", " ", 0, "0"];
    foreach($values as $value){
        if(is_string($value)){
            echo "{$value} is a string <br>";
        }
    }
    ?>
    <br>
    <br>
    <h3>gzcompress()</h3>
    <p>Compress a string</p>
    <?php
    $string = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus at ducimus repudiandae vel. Consequatur
        fugiat illum sit tenetur velit. Accusamus aperiam deserunt, minima obcaecati porro tenetur voluptas voluptatem.
        Alias asperiores doloribus earum, eveniet nostrum odit optio praesentium quia ratione veniam! Et laudantium
        magnam neque nostrum provident quia quidem sit unde voluptates. Corporis dicta dolorum facilis fuga hic id odit
        qui quia repellat temporibus. A aliquam aperiam atque, aut blanditiis cum cumque cupiditate debitis dolores
        ducimus eius excepturi exercitationem id incidunt ipsam ipsum iste molestiae mollitia nesciunt officiis pariatur
        quam qui quidem recusandae repellat sequi similique soluta sunt suscipit tempora tempore tenetur vero voluptas!
        Deserunt eligendi, libero nam quo sequi suscipit? Commodi non porro reprehenderit saepe tenetur! Alias,
        aspernatur commodi debitis delectus eaque enim error esse facere id illum impedit iure laborum laudantium,
        magnam officiis provident quis sint sit totam ullam veniam voluptatem voluptatum. Consequuntur, dolorum,
        placeat! Animi aperiam architecto aut autem cum deleniti dicta est et fugit illo ipsam itaque iusto molestiae
        molestias nesciunt nihil non nostrum odio optio pariatur quae quaerat quidem quo quod recusandae repudiandae
        rerum saepe sed, similique sit sunt unde voluptate voluptatem! Assumenda consectetur dignissimos doloribus.
        Dolore excepturi expedita illo inventore labore, magnam, magni maiores modi quia quis rem reprehenderit tempore
        voluptatibus? Accusantium at autem doloribus omnis provident! Alias aspernatur aut dicta dolorem ducimus eius
        eos illum ipsum libero maxime, minus molestiae officiis porro quam quia quod, reiciendis sapiente sed soluta
        voluptatibus. Ad commodi cumque debitis dolorem, dolorum eligendi exercitationem, incidunt iure, nam quasi
        repudiandae sit velit vitae. Aperiam beatae, cupiditate error itaque magni pariatur placeat reiciendis
        repudiandae sunt. Aliquam aliquid assumenda blanditiis commodi consectetur cupiditate deleniti deserunt dicta,
        dignissimos distinctio dolor dolore doloremque ducimus eum expedita facere facilis harum illo illum magnam modi
        natus nihil placeat quaerat quas quibusdam recusandae sequi similique sit, tenetur, ut veritatis voluptate
        voluptatem. Et fugit iusto nisi voluptates! Atque blanditiis, dicta earum, esse fuga fugit ipsa iusto molestias
        odio quasi qui quos ratione repellendus! Eum illo in iusto omnis provident sed unde voluptatum. Assumenda autem
        commodi consequatur cupiditate delectus dolorem dolorum eaque eos ex fuga fugiat harum in iste iusto laboriosam
        laborum laudantium maxime minus molestias nemo neque nobis obcaecati omnis provident ratione rem sequi tenetur
        unde, vel voluptate. Corporis ducimus enim excepturi, inventore laudantium numquam sint tempora? Accusamus amet
        consequatur corporis cum dolor eum excepturi labore laboriosam magni maiores modi molestiae nemo nesciunt
        obcaecati officia provident quaerat rem repudiandae sed sint, totam vel voluptatem. Amet aut corporis
        exercitationem facilis harum laborum laudantium magni minima minus, molestias optio pariatur, perferendis
        quibusdam sit sunt. Amet assumenda delectus ducimus enim id magni minima natus nihil nostrum nulla optio, quam
        quasi sit, tempora voluptates. Ad adipisci alias debitis, doloremque doloribus earum esse ipsam ipsum
        necessitatibus non quaerat quas quasi ratione repellat, rerum tempora unde vero voluptas. Beatae delectus
        deserunt earum est fugit minima neque nobis non, numquam officia, possimus qui sed sequi sint voluptates. Eum
        libero optio perferendis quo rem, unde ut voluptatibus. Beatae, dicta dolore harum minus molestias odio porro
        sed sit!";
$compressed = gzcompress($string);
    ?>
    <br>
    <h4>Output compressed string</h4>
    <?php
    echo $compressed;
    ?>
</div>
</body>
</html>