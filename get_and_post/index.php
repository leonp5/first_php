<?php include "server.php"; ?>
<!doctype html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Get and Post</title>
</head>
<body>
<div class="container">
    <h2>Get and Post requests</h2>
    <p>PHP Front To Back [Part 11] - Get & Post Tutorial</p>
    <a href="https://www.youtube.com/watch?v=cIFUH3Qnd6s&list=PLillGF-Rfqbap2IB6ZS4BBBcYPagAjpjn&index=11">Link</a>
    <h3>GET</h3>

    <div>
        <ul>
            <li>
                <a href="index.php?name=Leon">Leon</a>
            </li>
            <li>
                <a href="index.php?name=Tom">Tom</a>
            </li>
        </ul>
        <?php if($count > 0):?>
            <h2><?php echo "{$submitted["name"]}'s Profile"; ?></h2>
        <?php endif ?>
    </div>

<form method="GET" action="index.php">
    <div>
        <label>Name</label><br>
        <input type="text" name="name">
    </div>
    <div>
        <label>Email</label><br>
        <input type="text" name="email">
    </div>
    <input type="submit" value="Submit">
</form>
    <h3>POST</h3>
<form method="POST" action="index.php">
    <div>
        <label>Name</label><br>
        <input type="text" name="name">
    </div>
    <div>
        <label>Email</label><br>
        <input type="text" name="email">
    </div>
    <input type="submit" value="Submit">
</form>
<?php if($count > 0):?>
<h3>GET</h3>
<h4>Name: <?php echo $submitted["name"]?></h4>
<?php if(array_key_exists("email", $submitted)): ?>
        <h4> Email: <?php echo $submitted["email"]?></h4>
<?php endif ?>
<?php endif ?>
    <?php if($countedPost > 0):?>
<h3>POST</h3>
<h4>Name: <?php echo $posted["name"]?></h4>
<h4>Email: <?php echo $posted["email"]?></h4>
<?php endif ?>

</div>
</body>
</html>