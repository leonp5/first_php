<?php include 'inc/system_info.php'; ?>
<!doctype html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>System Info</title>
</head>
<body>
<div class="container">
    <h1>Server & File Info</h1>
    <?php if($server): ?>
    <ul class="list-group">
        <?php foreach ($server as $key => $value): ?>
        <li class="list-group-item">
            <strong>
                <?php echo $key ?>:
                <?php echo $value; ?>
            </strong>
        </li>
        <?php endforeach; ?>
    </ul>
    <?php endif; ?>

    <h1>Client Info</h1>
    <?php if($client): ?>
        <ul class="list-group">
            <?php foreach ($client as $key => $value): ?>
                <li class="list-group-item">
                    <strong>
                        <?php echo $key ?>:
                        <?php echo $value; ?>
                    </strong>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
    <br>
<p>PHP Front To Back [Part 10] - $_SERVER Superglobal</p>
<a href="https://www.youtube.com/watch?v=oVJ0anq8yLA&list=PLillGF-Rfqbap2IB6ZS4BBBcYPagAjpjn&index=10">Link</a>
</div>

</body>
</html>