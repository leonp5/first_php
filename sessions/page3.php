<?php
session_start();

$name = isset($_SESSION["name"]) ? $_SESSION["name"] : "Gast";
$email = isset($_SESSION["email"]) ? $_SESSION["email"] : "Emailadresse existiert nicht";

?>

<!doctype html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <title>Sessions - Seite 3</title>
</head>
<body>
<div class="container">
   <h4>Willkommen auf Seite 3 "<?php echo $name; ?>"</h4>
    <br>
    <a href="destroy.php">Session löschen</a>
</div>
</body>
</html>
