<?php
$msg = '';
$msgClass = '';

if(isset($_POST["submit"])){

    $name = htmlentities($_POST["name"]);
    $email = htmlentities($_POST["email"]);

    if(!empty($name) && !empty($email)) {
        session_start();

        $_SESSION["name"] = $name;
        $_SESSION["email"] = $email;

        header("Location: page2.php");
    } else {
        $msg = "Alle Felder müssen ausgefüllt werden";
        $msgClass = "alert-danger";
    }
}

?>

<!doctype html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Sessions</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php">PHP Sessions</a>
            </div>
        </div>

    </nav>
    <br>
    <br>
<div class="container">
    <p>PHP Front To Back [Part 16] - Sessions Tutorial</p>
    <a href="https://www.youtube.com/watch?v=W4rSS4-LdIE&list=PLillGF-Rfqbap2IB6ZS4BBBcYPagAjpjn&index=16">Link</a>
<br>
<br>
<br>
<div class="container">
    <?php if($msg != ""): ?>
        <div class="alert <?php echo $msgClass?>"> <?php echo $msg; ?> </div>
    <?php endif; ?>
    <form method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
        <div class="form-group">
            <label>Name</label>
            <input type="text" name="name" class="form-control">
        </div>
        <div>
            <label>Email</label>
            <input type="text" name="email" class="form-control">
        </div>
        <br>
        <button type="submit" name="submit" class="btn btn-primary">Abschicken</button>
    </form>

</div>
</body>
</html>