<?php
session_start();

$name = isset($_SESSION["name"]) ? $_SESSION["name"] : "Gast";
$email = isset($_SESSION["email"]) ? $_SESSION["email"] : "Emailadresse existiert nicht";


?>

<!doctype html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <title>Sessions - Seite 2</title>
</head>
<body>
<div class="container">
<h5>Vielen Dank "<?php echo $name; ?>"! Du hast dich mit folgender Emailadresse eingetragen: <?php echo $email; ?></h5>
    <a href="page3.php">Weiter zu Seite 3</a>
</div>
</body>
</html>