<!doctype html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Filters and validation</title>
</head>
<body>
<div class="container">
    <h2>Filters and validation</h2>
    <p>PHP Front To Back [Part 14] - Filters & Validation</p>
    <a href="https://www.youtube.com/watch?v=pfY9LwcsH3A&list=PLillGF-Rfqbap2IB6ZS4BBBcYPagAjpjn&index=14">Link</a>
<br>
<br>
<br>
<br>
<h3>filter_input_array()</h3>

    <?php

    #FILTER_VALIDATE_BOOLEAN
    #FILTER_VALIDATE_EMAIL
    #FILTER_VALIDATE_FLOAT
    #FILTER_VALIDATE_INT
    #FILTER_VALIDATE_IP
    #FILTER_VALIDATE_REGEXP
    #FILTER_VALIDATE_URL

    #FILTER_SANITIZE_EMAIL
    #FILTER_SANITIZE_ENCODED
    #FILTER_SANITIZE_NUMBER_FLOAT
    #FILTER_SANITIZE_NUMBER_INT
    #FILTER_SANITIZE_SPECIAL_CHARS
    #FILTER_SANITIZE_STRING
    #FILTER_SANITIZE_URL

    // Check for posted data
//    if(filter_has_var(INPUT_POST, "data")) {
//        echo "Yes!!! There are some data!";
//    } else {
//        echo "Sadly no data";
//    }
//    if(filter_has_var(INPUT_POST, "data")){
//        $email = $_POST["data"];
//
//        // Remove illegal chars
//
//        $email = filter_var($email, FILTER_SANITIZE_EMAIL);
//        echo $email . "<br>";
//
//        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
//            echo "Email is valid";
//        } else {
//            echo "Invalid email";
//        }
//    }


    $filters = [
            "data" => FILTER_VALIDATE_EMAIL,
        "data2" => [
                "filter" => FILTER_VALIDATE_INT,
            "options" => [
                    "min_range" => 1,
                "max_range" => 100
            ]
        ]
    ];

    print_r(filter_input_array(INPUT_POST, $filters));
    ?>

<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <input type="text" name="data">
    <input type="text" name="data2">
    <button type="submit">Submit</button>
</form>
<br>
<br>
<br>
    <h3>Drei inputs</h3>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="name">Name</label>
        <input type="text" name="name">
        <label for="age">Age</label>
        <input type="text" name="age">
        <label for="email">Email</label>
        <input type="text" name="email">
        <button type="submit">Submit</button>
    </form>
    <br>
    <br>
    <?php
//    $arr = [
//            "name" => "Friedrich Nietzsche",
//        "age" => "32",
//        "email" => "fritz@test.de"
//    ];

    $filters2 = [
            "name" => [
                    "filter" => FILTER_CALLBACK,
                "options" => "ucwords"
            ],
        "age" => [
                "filter" => FILTER_VALIDATE_INT,
            "options" => [
                    "min_range" => 1,
                "max_range" => 121
            ]
        ],
        "email" => FILTER_VALIDATE_EMAIL
    ];

    $countedInput = count(array_filter($_POST));
    if($countedInput > 0){
        foreach ($_POST as $inputName => $value){
            if($value != "") {
                echo "Inputfield: ". $inputName . "<br>";
                echo "Input: " . $value . "<br> <br>";
            } else {
                echo "Inputfield " . "'$inputName'" . " is empty" . "<br> <br>";
            }
        }
    print_r(filter_var_array($_POST, $filters2));

    } else {
        echo "Please insert data";
    }

    ?>
</div>
</body>
</html>