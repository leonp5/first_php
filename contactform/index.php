<?php

// Message Vars
$msg = '';
$msgClass = '';

if(filter_has_var(INPUT_POST, "submit")) {
    $name = htmlspecialchars($_POST["name"]);
    $email = htmlspecialchars($_POST["email"]);
    $message = htmlspecialchars($_POST["message"]);


    // Check required fields
    if (!empty($email) && !empty($name) && !empty($message)) {
        // Check email
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            $msg = "Gültige Emailadressse eintragen";
            $msgClass = "alert-danger";
        } else {
        $msg = "Input ist angekommen";
        $msgClass = "alert-success";

        // Send mail
            $recipient = "leonpe@web.de";
            $subject = "Anfrage von " .$name;
            $body = "<h2>Kontaktanfrage</h2> 
                    <h4>Name</h4><p>".$name."</p>
                    <h4>Email</h4><p>".$email."</p>
                    <h4>Nachricht</h4><p>".$message."</p>";

            // Email headers
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-Type:text/html;charset=UTF-8" . "\r\n";
            $headers .= "From: " .$name. "<" .$email. ">" . "\r\n";

            if(mail($recipient, $subject, $body, $headers)){
                $msg = "Mail gesendet";
                $msgClass = "alert-success";
            } else {
                $msg = "Mail konnte nicht gesendet werden";
                $msgClass = "alert-danger";
            }

        }
    } else {
        $msg = "Bitte alle Felder ausfüllen!";
        $msgClass = "alert-danger";
    }
}
?>

<!doctype html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Filters and validation</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php">PHP Contact form tutorial</a>
            </div>
        </div>

    </nav>
    <br>
    <br>
<div class="container">
    <p>PHP Front To Back [Part 15] - PHP Contact Form</p>
    <a href="https://www.youtube.com/watch?v=tJ5eUgOxITE&list=PLillGF-Rfqbap2IB6ZS4BBBcYPagAjpjn&index=15">Link</a>
<br>
<br>
<br>
<div class="container">
    <?php if($msg != ""): ?>
<div class="alert <?php echo $msgClass?>"> <?php echo $msg; ?> </div>
    <?php endif; ?>
    <form method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
    <div class="form-group">
        <label>Name</label>
        <input type="text" name="name" class="form-control" value="<?php echo isset($_POST["name"]) ? $name : ""; ?>">
    </div>
    <div>
        <label>Email</label>
        <input type="text" name="email" class="form-control" value="<?php echo isset($_POST["email"]) ? $email : ""; ?>">
    </div>
    <div>
        <label>Nachricht</label>
        <textarea name="message" class="form-control" ><?php echo isset($_POST["message"]) ? $message : ""; ?></textarea>
    </div>
    <br>
    <button type="submit" name="submit" class="btn btn-primary">Abschicken</button>
    </form>
</div>

</div>
</body>
</html>